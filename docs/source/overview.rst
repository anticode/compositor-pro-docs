Overview
========
Compositor Pro is a Blender addon that provides state of the art effects and tools for the Blender Compositor.

Installation
------------
Like any other `add-on <https://docs.blender.org/manual/en/latest/editors/preferences/addons.html>`_, to install Compositor Pro go to your Blender :doc:`preferences` and click the install button in the Add-ons tab.

Add-on Preferences
------------------
Compositor Pro comes with several useful settings that help make the experience smoother. Most of these settings are fairly basic and don't make huge changes to the add-on experience itself. For a brief description of the options, you can hover your cursor over them or go to the :doc:`preferences` page.

New Features
------------
To discover all of the powerful features Compositor Pro has to offer, check out the :doc:`features` page.

New Nodes
---------
Compositor Pro has a large selection of nodes to choose from. To see what they're all about, check out the :doc:`nodes` page.

.. note::
    These docs have not been finished yet.