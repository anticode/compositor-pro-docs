Preferences
===========
Preferences are options that change how Compositor Pro functions. Most of these are small changes.

Add-on Panel
------------
* Quick Add changes the node selector menu on the `N panel <https://docs.blender.org/manual/en/latest/interface/window_system/tabs_panels.html>`_ to instantly add a node instead of needing the Add Node button to be pressed. This is useful for working quickly, but might be disruptive when accidentally clicking on the wrong node.
* Invert Mix Order inverts the order that nodes need to be selected when using the :doc:`features/add_mix_node` feature.
* Use Colored Nodes gives imported nodes a customized color based on their category.
    * These colors can be modified in the Other Options.
* Thumbnail Size changes the size of preview thumbnails when opening the node selector. This can be useful if you need to get a better look at them, though Blender has issues with rendering the full thumbnail resolution at large thumbnail sizes.
* Node Width changes the width of nodes when they are imported by Compositor Pro. This is just a personal perference change.

Keyboard Shortcutes
-------------------
* The keybind for the :doc:`features/radial_menu`.

Other Options
-------------
.. note::
    These options are not useful for normal users and are not documented beyond this.
* Dev Tools are a collection of nodes that are not typically visible to the user and aren't really useful for normal users.
* Developer Insights reveals hidden developer information that's only useful for developing Compositor Pro. 