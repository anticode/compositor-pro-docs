Usage
=====
All of Compositor Pro's tools are accessed from Blender's Compositor window. There are two main ways to locate these tools, on the side panel (usually on the right one) and in the :doc:`features/radial_menu` (which is bound to ``V`` by default and can be changed from the :doc:`preferences` menu).