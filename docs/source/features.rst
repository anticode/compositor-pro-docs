Features
========
Along with its amazing nodes, Compositor Pro also comes with incredibly useful features - some of which might not be easy to understand. 

.. toctree::
    features/optimization_menu
    features/node_selector
    features/favorites
    features/custom_nodes
    features/add_mix_node
    features/color_grading
    features/radial_menu