Nodes
=====
Nodes are the bread and butter of Blender's `Compositor <https://docs.blender.org/manual/en/latest/compositing/index.html>`_ window and are the way that you get any compositing done at all. Compositor Pro has a massive collection of state of the art nodes that are designed to be accurate, fast, and useful.

Mixed Effects
-------------
Nodes that have their effects mixed in with the output of the node, similar to how you would expect a built-in node to behave.

.. toctree::
    :maxdepth: 1
    :glob:

    nodes/mixed/*

Unmixed Effects
---------------
Nodes that only output the effect. These effects are expected to be mixed by the user and, although any mixing method works, Compositor Pro provides its own mixing method in the form of the :doc:`features/add_mix_node` feature.

.. toctree::
    :maxdepth: 1
    :glob:

    nodes/unmixed/*

Color
-----
Nodes that alter the color of the image. These nodes are most useful for color grading operations.

.. toctree::
    :maxdepth: 1
    :glob:

    nodes/color/*

Batches
-------
Nodes that are preset groups of effects that are compiled together. These nodes are groups of smaller nodes that are put together for you. You can use these nodes to help you learn some useful ways of using Compositor Pro to your advantage.

.. toctree::
    :maxdepth: 1
    :glob:

    nodes/batches/*

Utilities
---------
Nodes that do utility operations that are useful in compisiting but do not have an effect on their own. You should, primarily, use these to make effects and nodes of your own.

.. toctree::
    :maxdepth: 1
    :glob:
    
    nodes/utilities/*