What's New?
===========
This is where you can find patch notes for Compositor Pro. Brand new features, fixes, and changes will all be listed here so you know what changes between versions.

Version 1.0
-----------
We've been working on version 1.0 for a very long time and with its release, we're excited to be at a point where the add-on could be considered "feature-complete." This doesn't mean we're going to stop working on features, of course, just that we've finally implemented all of the benchmark features we felt had to be included before Compositor Pro could be considered complete. We wouldn't have gotten here without your support and we definitely wouldn't have gotten here without your help finding bugs and creating new features. That said, let's get into the actual notes.

General:

* Support for Blender 4.1
* Slight performance optimizations across the add-on.
* Slight space optimizations, so it doesn't take up more space on the disk than it should.

Nodes:

* Batch nodes now have new inputs to help give you more options with the default toolsets.

**NEW** :doc:`features/custom_nodes`:

* Create a node group in the compositor and press the Add Custom Node button to add it to the new Custom Nodes category in the :doc:`features/node_selector`.
* Use the refresh button in case your custom nodes are desynced.

UI Overhaul:

* The add-on has received a complete UI overhaul across the board. It should be more pretty and usable.
* Nodes now have descriptions that can be seen if you hover over them in the Node Selector.
* As part of this overhaul, nodes that are added to the compositor via the Node Selector are now colored based on their category.
    * This should help differentiate Compositor Pro nodes from regular node groups or Blender's default nodes.
    * It should also help you find your place in large node trees.
    * This feature can be disabled in your :doc:`preferences`. You may also select your own custom colors from Preferences as well.
* Node Width option in your Preferences now lets you select the default width of Compositor Pro nodes when they are imported.

Documentation:

* Documentation button has been added to the Preferences menu.
* To the right of the add node button is an information button, which will take you to the documentation page for the button in question.
* Just in case you might have missed it, a documentation button has also been added to the top left of the N panel.

:doc:`features/radial_menu`:

* Can now show custom nodes as well as your favorite nodes for quick importing.

Fixes:

* Fixed an issue where being in any non-object mode in your 3D view could cause issues with Compositor Pro when importing a new node.
    * I still don't understand this one, but... Okay.
* Fixed an issue where Compositor Pro could accidentally spam errors into Blender's console. 
* Fixed an issue where Compositor Pro could crash Blender completely on some instances of Blender 4.1.
    * Realistically, this should have crashed Blender for EVERYONE, but for some reason it didn't. Either way, its fixed for everyone.
* Fixed a lock that can occur in some files, preventing you from being able to add any nodes.
* A warning is now included when trying to open Compositor Pro's radial menu in a non-Compositor node tree.