Depth Fog
=========
:doc:`../../nodes`

Adds a depth-based fog effect, using depth as an input.

Inputs
------
* Depth (Render Depth)
* Minimum Distance (Float)
    * Determines the minimum distance of the depth effect.
* Maximum Distance (Float)
    * Determines the maximum distance of the depth effect.
* Exponent (Float)
    * Determines the falloff of the depth effect by x^2.
* Color (Color)
    * Determines the color of the depth effect.