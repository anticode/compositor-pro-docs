Outline Mask
============
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Generates an outline mask using Sobel edge detection from the input.

Inputs
------
* Radius (Float)
    * Determines the radius of the outline.
* Outline Minimum (Float)
    * Determines the minimum value to be used for the outline mask.
* Outline Maximum (Float)
    * Determines the maximum value to be used for the outline mask.
* Intensity (Float)
    * Intensity of the outline mask.