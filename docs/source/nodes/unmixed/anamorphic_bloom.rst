Anamorphic Bloom
================
:doc:`../../nodes`

Adds a bloom effect similar to glare seen in anamorphic lenses. It uses a 5-step box blur that is applied along the Y axis (left-right).

Inputs
------
* Exposure (Float)
    * Adjusts the exposure of the output of the node.
    * Similar to scene exposure in Blender.
* Scale (Float)
    * Scale of the bloom along the Y axis.
* Weight (Float)
    * Weight determines the mix bias of the blur chain.
    * Larger weight means a tighter, punchier bloom.
        * Larger weight means the smaller scale blurs will be mixed in more.
    * Smaller weight is a softer, larger bloom.
        * Smaller weight means the larger scale blurs will be mixed in more.