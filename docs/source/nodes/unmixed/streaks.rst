Streaks
=======
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Adds a multidirectional streak effect seen in digital sensors. The same as Glint but the size of the effect is much larger.

Inputs
------
* Exposure (Float)
    * Adjusts the exposure of the output of the node.
    * Similar to scene exposure in Blender.
* Streak Dispersion (Float)
    * Determines the amount of dispersion effect on the streaks.