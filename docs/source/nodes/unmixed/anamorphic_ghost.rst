Anamorphic Ghost
================
:doc:`../../nodes`

Adds a ghosting effect that is along the Y axis (left-right) of the image. This is similar to ghosting seen in anamorphic lens flares and anamorphic lenses.

Inputs
------
* Exposure (Float)
    * Adjusts the exposure of the node.
    * Similar to scene exposure in Blender.
* Size (Float)
    * Size and offset of different elements of the ghost.