Bloom
=====
:doc:`../../nodes`

Adds a bloom effect seen in lenses, cameras, and even human eyes. This bloom effect uses a 10-step Gausian blur that is applied separately to the X and Y axes.

Inputs
------
* Scale X (Float)
    * Determines the scale of the bloom along the X axis.
* Scale Y (Float)
    * Determines the scale of the bloom along the Y axis.
* Weight (Float)
    * Determines the mix bias of the blur chain.
    * Larger weight means a tighter, punchier bloom.
        * Larger weight means the smaller scale blurs will be mixed in more.
    * Smaller weight is a softer, larger bloom.
        * Smaller weight means the larger scale blurs will be mixed in more.
* Exposure (Float)
    * Adjusts the exposure output of the node.
    * Similar to scene exposure in Blender.