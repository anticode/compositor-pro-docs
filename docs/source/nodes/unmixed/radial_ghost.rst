Radial Ghost
============
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Adds a ghost effect that is specifically seen along the edges of a lens.

Inputs
------
* Input Scale (Float)
    * Scale of the input that will generate the ghosts.
* Radial Scale (Float)
    * Scale of the output from the distorted radial effect.
* Radial Distortion (Float)
    * Amount of radial distortion to use before generating the ghost.
* Starburst (Float)
    * Overlays a starburst texture over the ghosts.
* Dispersion (Float)
    * Amount of dispersion applied to the ghosts.
* Bokeh (Float)
    * Amount of bokeh blur applied to the ghosts.
* Bokeh Image (Image)
    * Shape of the bokeh to be used in the ghosts
* Exposure (Float)
    * Adjusts the exposure of the output of the node.
    * Similar to scene exposure in Blender.
* Shape (Float)
    * Determines the shape of the dispersion effect. 
    * 0 means radial dispersion.
    * 1 means dispersion along the Y axis (left-right).
    * -1 means dispersion along the X axis (up-down).