Ghost
=====
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Adds an effect similar to various ghost elements seen in lens flares. This effect creates 3 separate ghosts.

Inputs
------
* Input Scale (Float)
    * Scale of the input that will generate the ghosts.
* Radial Scale (Float) 
    * Scale of the output from the distorted radial effect.
* Radial Distortion (Float)
    * Amount of radial distortion to use before generating the ghost.
* Squeeze (Float)
    * Amount of anamorphic squeeze to apply before generation the ghost. 
    * Higher values will create more squeezed result.
    * Decimal values will create stretched results.
* Interference (Float) 
    * Determines the distance between the 3 different ghost elements.
* Starburst (Float)
    * Overlays a starburst texture over the ghosts
* Dispersion (Float)
    * Amount of dispersion applied to the ghosts
* Bokeh (Float)
    * Amount of bokeh blur applied to the ghosts
* Bokeh Image (Texture)
    * Shape of the bokeh to be used in the ghosts
* Centre Masking (Float)
    * Masks the centre of the image to not have ghosts. 
    * Higher values increase the scale of the mask.
* Exposure (Float)
    * Adjusts the exposure of the output of the node.
