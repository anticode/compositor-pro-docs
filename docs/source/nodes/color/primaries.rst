Primaries
=========
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Alter the primary red, green, and blue colors and remaps them to something else.

Inputs
------
* Red Primary (Color)
    * Remaps red to this color.
* Blue Primary (Color)
    * Remaps blue to this color.
* Green Primary (Color)
    * Remaps green to this color.