TanH View Transform
===================
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Applies the TanH math function to all 3 channels, working as a psuedo view transform compressor.