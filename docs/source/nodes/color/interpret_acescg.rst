Interpret ACEScg
================
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Interprets the incoming input as if it was ACEScg linear space.