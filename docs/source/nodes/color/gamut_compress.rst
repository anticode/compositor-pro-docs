Gamut Compress
==============
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Compresses the gamut of the image using a gamut compression algorithm.

Inputs
------
* Limit (Float)
    * Max limit of the gamut of the image.
* Threshold (Float)
    * Determines the thresholds for gamut compression.
* Power (Float)
    * Determines the power of the falloff to max gamut.