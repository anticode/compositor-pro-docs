Chroma
======
:doc:`../../nodes`

Alters the saturation/chroma of the image using a power function for a smoother falloff. Makes sure that saturated colors are not adjusted at the same rate.

Inputs
------
* Chroma (Float)
    * Amount of saturation alteration in the image.
    * Higher values mean more saturation.
    * 1 means default.