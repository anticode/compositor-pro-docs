Linear Exponent
===============
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

An exponent function that works on linear inputs by determining a middle grey value.

Inputs
------
* Exponent (Float)
    * Amount of power to be applied on the input.
* Exponent Color (Color)
    * Color of the exponent to be applied on the input.
* Mid Grey (Float)
    * Determines the mid grey value of your image. Value of 0.18 is recommended.