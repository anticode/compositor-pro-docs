Linear Overlay
==============
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Linear overlay is a blend mode similar to overlay, but is designed to work with HDR images.

Inputs
------
* Threshold (Float)
    * Values below this threshold will be considered the "darker" part of the image when doing the overlay operation.