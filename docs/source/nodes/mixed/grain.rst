Grain
=====
:doc:`../../nodes`

Grain mixes in a small grain pattern to the image to emulate film grain. The grain pattern is both mixed and multiplied over the image. There is also a psuedo grain mosaic effect to make the effect appear more natural at the cost of making the underlying image appear slightly blurry.

You can use the :doc:`../../features/grain_replacer` to replace the grain texture used by the Grain node.

Inputs
------

* Mix (Float)
    * Determines how much the grain effect will be mixed in.
    * Similar to Fac
* Size (Float)
    * Scale of the grain pattern.
    * Smaller values means a finer pattern.
* Bias (Float)
    * Determines which parts of the image while be more grainy.
    * Lower values mean that shadows will get more grain.
    * Higher values mean that highlights will get more grain.
    * Bias of 0.5 means that grain is applied equally over the image.
* Midlevel (Float)
    * Determines the midlevel of the grain pattern.
    * For HDR images, a midlevel above 1.0 is fine but for LDR images a midlevel of 0.5 is recommended.
* Contrast (Float)
    * The range of values in a grain pattern.
    * This determines variance from the midlevel, rather than total distribution.
    * A value of 0.5 is recommended for LDR images.
* Mosaic (Float)
    * Determines how much of the mosaic effect is mixed.
    * Similar to Fac
* Mosaic Blur (Float)
    * How much blur is applied before the mosaic effect.
