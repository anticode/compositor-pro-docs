Chroma Blur
===========
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Chroma blur is an effect that will blur the "color" of the image but not the value/luminance of the image. This effect is typically seen on CRT monitors or VHS recordings. This will create an image that has blurry color and saturation, but the image remains sharp.

Inputs
------

* Size X (Float)
    * Determines the size of the blur along the X axis (horizontally).
* Size Y (Float)
    * Determines the size of the blur along the Y axis (vertically).