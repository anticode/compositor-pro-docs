Halation
========
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Halation is a colored blur/bloom effect applied to specific colors of the image to emulate the halation effect seen in film. Its usually applied to red, orange, or yellow parts of the image.

Inputs
------

* Halation Color (Color)
    * Determines the color of the halation effect.
* Threshold (Float)
    * Any values below this threshold will not have the halation effect applied to it.
* Exposure (Float)
    * Adjusts the exposure of the output of the node.
    * This works similar to scene exposure in Blender.
* Fac (Float)
    * Determines how much of the effect is mixed in.
* Size (Float)
    * Determines the size of the halation effect.