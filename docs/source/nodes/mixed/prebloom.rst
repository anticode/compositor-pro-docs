Prebloom
========
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Prebloom applies common operations that are sometimes required before effects like bloom and glare. These options include thresholding and dispersion.

Inputs
------
* Threshold (Float)
    * Values below the threshold will be negated
* Threshold Knee (Float)
    * Determines the falloff between the negated threshold values and the accepted values.
* Dispersion (Float)
    * Determines how much of the dispersion effect will be used.
* Dispersion Scale (Float)
    * Determines the scale of the dispersion effect.
* Dispersion Shape (Float)
    * Determines the shape of the dispersion effect.
    * 0 means radial dispersion.
    * 1 means dispersion on the Y axis.
    * -1 means dispersion on the X axis.
