CRT Grid
========
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

CRT Grid vaguely emulates the grid pattern of the CRT monitors. It will only emulate the pattern of the CRT and not the colors and other aspects.

Inputs
------

* Grid Displacement (Float)
    * Determines the interlacing pattern of the grid columns.
    * 0 means no displacement; the grid will be uniform.
* Shadowing (Float)
    * Adds shadowing in between the grid.
    * 0 means no shadows.
* Exposure (Float)
    * Adjusts the exposure of the output node.
    * This works similar to scene exposure in Blender.