Sharpen
=======
:doc:`../../nodes`

Adds a sharpening process to the image, done in logarithmic space to avoid artifacts.

Inputs
------
* Intensity (Float)
    * Intensity of the sharpening effect.
* Scale (Float)
    * Scale of the sharpening effect.
* Clamp (Float)
    * Clamps the range of the sharpening effect.
    * Lower clamp values means a more uniform sharpening across the board.
    * 0 disables the effect.