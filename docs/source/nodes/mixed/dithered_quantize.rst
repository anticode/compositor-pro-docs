Dithered Quantize
=================
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Dithered quantize will quantize, or reduce the bit depth of, the input image with dithering. This removes the banding artifact introduced by regular quantize operations. The dithering pattern is an 8x8 matrix.

Inputs
------

* Bit Depth (Float)
    * The amount of bits that will be present.
    * The lower the number the less amount of colors will be visible.
* Dithering (Float)
    * How much dithering to apply to the image.
    * 0 is undithered.
* Retain Hue (Float)
    * Try to retain the hue of every pixel.
    * Helps retain color definition in lower bits.
    * This will invalidate the tchnical bit depth reduction; this is meant to be an artistic tool.