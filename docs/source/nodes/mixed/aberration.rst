Aberration
==========
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Aberration is an effect that creates 2 different aberrations along the X axis of your image. It is reminiscent of fringes seen in real lenses that are commonly colored orange, blue, green, or purple.

Inputs
------

* Color (Color)
    * The color of the aberration effect. 
    * The 2 aberrations will always be the opposite color of each other. 
    * This color is the one used for the right aberration; the left is automatically calculated.
* Threshold (Float)
    * Any values below the threshold will not have the aberration effect applied to it.
    * It is recommended to have a threshold above 0.18.
* Size (Float)
    * Determines the size, or width, of the aberration effect.
* Fac (Float)
    * Determines how much the aberration effect will be mixed in.