Haze
====
:doc:`../../nodes`

Haze is a large scale blur applied to the image to imitate a hazy lens or filter.

Inputs
------

* Haze color (Color)
    * Determines the color of the haze effect.
* Fac (Float)
    * Determines how much of the effect is mixed.
* Size (Float)
    * Determines the size of the effect.
* Exposure (Float)
    * Adjusts the exposure of the output.
    * This is similar to scene exposure in Blender.