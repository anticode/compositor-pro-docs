Radial Defocus
==============
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Radial defocus adds an out of focus effect near the edges of the frame, similar to a detuned lens or an anamorphic lens.

Inputs
------
* Bokeh Texture (Image)
    * Texture of the bokeh shape to be used for the effect.
    * The built-in bokeh image node can be used for this.
        * Leaving this blank will create a square bokeh texture, which is not desireable.
* Scale (Float)
    * Scale of the mask that determines the blur.
* Falloff (Float)
    * Falloff for the mask that determines the blur.
* Blur Amount (Float)
    * Amount of defocus/blur on the effect.