Sensor Fringe
=============
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Adds a colored, purple blur effect that is similar to old or faulty CMOS sensor digital cameras.

Inputs
------
* Size (Float)
    * Determines the size of the blur.