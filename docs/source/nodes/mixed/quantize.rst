Quantize
========
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Quantize will quantize, or reduce the bit depth of, the image. What separates this node from the built-in node is retain hue function which will retain the hue of the image while quantizing. Using retain hue will not, technically, quantize the image; it is intended for artistic use.

Inputs
------
* Bit Depth (Float)
    * The amount of bits that will be present.
    * The lower this number the less amount of colors that will be visible.
* Retain Hue (Float)
    * Tries to retain the hue of every pixel.