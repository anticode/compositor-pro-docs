Scene Outlines
==============
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Adds an outline to the scene using depth and normal information. It uses Sobel edge detection to generate the edges.

Inputs
------
* Radius (Float)
    * Determines the radius of the outlines.
* Harshness (Float)
    * Determines the harshness of the generated outlines.
    * A value of 1 means sharp outlines, but prone to aliasing. 
* Depth Minimum (Float)
    * Minimum value to use for depth-based outlines.
* Depth Maximum (Float)
    * Maximum value to use for depth-based outlines.
* Normals Minimum (Float)
    * Minimum value to use for normal-based outlines.
* Normals Maximum (Float)
    * Maximum value to use for normal-based outlines.
* Depth Intensity (Float)
    * Intensity of the depth-based outlines.
* Normal Intensity (Float)
    * Intensity of the normal-based outlines.
* Outlines Color/Image (Color)
    * Determines the color or texture of the outline.