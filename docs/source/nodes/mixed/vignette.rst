Vignette
========
:doc:`../../nodes`

Adds a vignette effect similar to lens vignette from cameras.

Inputs
------
* Fac (Float)
    * Amount of vignette to be mixed.
* Scale (Float)
    * Scale of the vignette.
* Softness (Float)
    * Falloff of the vignette.
    * Lower value means sharper falloff.