Anamorphic Flare
================
:doc:`../../nodes`

Anamorphic Flare combines :doc:`../unmixed/anamorphic_bloom` and :doc:`../unmixed/anamorphic_ghost` together. Refer to those nodes for an explanation of their function and inputs.

Inputs
------
* :doc:`../unmixed/anamorphic_bloom`
    * Exposure (Float)
        * Adjusts the exposure of the output of the node.
        * Similar to scene exposure in Blender.
    * Scale (Float)
        * Scale of the bloom along the Y axis.
    * Weight (Float)
        * Weight determines the mix bias of the blur chain.
        * Larger weight means a tighter, punchier bloom.
            * Larger weight means the smaller scale blurs will be mixed in more.
        * Smaller weight is a softer, larger bloom.
            * Smaller weight means the larger scale blurs will be mixed in more.
* :doc:`../unmixed/anamorphic_ghost`
    * Exposure (Float)
        * Adjusts the exposure of the node.
        * Similar to scene exposure in Blender.
    * Scale (Float)
        * Size and offset of different elements of the ghost.
        * The Size of the Anamorphic Ghost node.