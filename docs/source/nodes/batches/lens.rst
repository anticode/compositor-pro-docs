Lens
====
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Lens is a combination of :doc:`../unmixed/bloom`, :doc:`anamorphic_flare`, :doc:`../unmixed/streaks`, :doc:`../unmixed/ghost`, :doc:`../unmixed/radial_ghost`, and :doc:`../mixed/sensor_fringe`.

Inputs
------
* :doc:`../unmixed/bloom`
    * Scale (Float)
        * Controls Scale X and Scale Y inputs.
        * Scale Y is multiplied by 0.5.
        * Determines the scale of the bloom.
    * Color (Color)
        * Discolors the bloom effect.
        * The color used in a Mix Color node.
        * The fac for this Mix Color node is 1.0.
    * Fac (Float)
        * Determines the factor used in the Mix node.
* Glare Dispersion (Float)
    * Determines the Dispersion of a :doc:`../mixed/prebloom` node used before the :doc:`anamorphic_flare` node.
* :doc:`anamorphic_flare`
    * :doc:`../unmixed/anamorphic_bloom`    
        * Scale (Float)
            * Scale of the bloom along the Y axis.  
    * :doc:`../unmixed/anamorphic_ghost`
        * Exposure (Float)
            * Adjusts the exposure of the node.
            * Similar to scene exposure in Blender.
        * Scale (Float)
            * Size and offset of different elements of the ghost.
            * The Size of the Anamorphic Ghost node.
    * Color (Color)
        * Discolors the anamorphic flare effect.
        * The color used in a Mix Color node.
        * The fac for this Mix Color node is 1.0.
    * Fac (Float)
        * Determines the factor used in the Mix node.
* :doc:`../unmixed/streaks`
    * Color (Color)
        * Discolors the streaks.
        * The color used in a Mix Color node.
        * The fac for this Mix Color node is 1.0.
    * Dispersion (Float)
        * Determines the factor used in the Mix node.
* :doc:`../unmixed/ghost` Intensity (Float)
    * Determines the factor used in the Mix node for the Ghost node.
* :doc:`../unmixed/radial_ghost` Intensity (Float)
    * Determines the factor used in the Mix node for the Radial Ghost node.
* Lens Distortion (Float)
    * Determines the Distort value of a :doc:`../utilities/uniform_distortion` node.