Tail-end
========
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Tail-end is a combination of :doc:`../mixed/aberration`, :doc:`../mixed/halation`, :doc:`../mixed/sharpen`, and :doc:`../mixed/grain`.

Inputs
------
* :doc:`../mixed/aberration`
    * Color (Color)
        * The color of the aberration effect. 
        * The 2 aberrations will always be the opposite color of each other. 
        * This color is the one used for the right aberration; the left is automatically calculated.
    * Threshold (Float)
        * Any values below the threshold will not have the aberration effect applied to it.
        * It is recommended to have a threshold above 0.18.
    * Size (Float)
        * Determines the size, or width, of the aberration effect.
    * Fac (Float)
        * Determines how much the aberration effect will be mixed in.
* :doc:`../mixed/halation`
    * Color (Color)
        * Determines the color of the halation effect.
    * Threshold (Float)
        * Any values below this threshold will not have the halation effect applied to it.
    * Fac (Float)
        * Determines how much of the effect is mixed in.
    * Size (Float)
        * Determines the size of the halation effect.
* :doc:`../mixed/sharpen`
    * Intensity (Float)
        * Intensity of the sharpening effect.
    * Scale (Float)
        * Scale of the sharpening effect.
* :doc:`../mixed/grain`
    * Fac (Float)
        * Determines the Mix of the Grain node.
        * Determines how much the grain effect will be mixed in.
    * Size (Float)
        * Scale of the grain pattern.
        * Smaller values means a finer pattern.
    * Contrast (Float)
        * The range of values in a grain pattern.
        * This determines variance from the midlevel, rather than total distribution.
        * A value of 0.5 is recommended for LDR images.