CRT Process
===========
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

CRT Process is a vague emulation of CRT footage using nodes like :doc:`../mixed/crt_grid`, :doc:`../mixed/chroma_blur`, :doc:`../color/primaries`, and :doc:`../mixed/vignette`.

Inputs
------
* Haze (Float)
    * Determines the size of the haze effect.
* Chroma Blur (Float)
    * Determines the Sixe X of :doc:`../mixed/chroma_blur`.
* :doc:`../mixed/sharpen`
    * Intensity (Float)
        * Intensity of the sharpening effect.
    * Scale (Float)
        * Scale of the sharpening effect.
* Vignette (Float)
    * Determines the Fac of :doc:`../mixed/vignette`.