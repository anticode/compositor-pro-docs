Generic Comp
============
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Generic Comp is a combination of :doc:`../unmixed/bloom`, :doc:`../unmixed/streaks`, :doc:`../mixed/sharpen`, and :doc:`../mixed/grain`.

Inputs
------
* :doc:`../unmixed/bloom`
    * Scale (Float)
        * Controls Scale X and Scale Y inputs.
        * Determines the scale of the bloom.
    * Fac (Float)
        * Determines the factor used in the Mix node.
* :doc:`../mixed/sharpen`
    * Intensity (Float)
        * Intensity of the sharpening effect.
    * Scale (Float)
        * Scale of the sharpening effect.
* :doc:`../mixed/grain`
    * Fac (Float)
        * Determines the Mix of the Grain node.
        * Determines how much the grain effect will be mixed in.
    * Size (Float)
        * Determines the Size of the Grain node.
        * Scale of the grain pattern.
        * Smaller values means a finer pattern.