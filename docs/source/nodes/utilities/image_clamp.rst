Image Clamp
===========
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Clamps the maximum and minimum values of the input image.

Inputs
------
* Max Clamp (Float)
    * Determines the maximum value to be clamped.
* Min Clamp (Float)
    * Determines the minimum value to be clamped.