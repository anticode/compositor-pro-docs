Unsharp Mask
============
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Creates an unsharp mask based on the input. Usually used for a sharpening effect or as edge detection.

Inputs
------
* Radius (Float)
    * Determines the radius of the unsharp effect.