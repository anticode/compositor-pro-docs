RGB to Luminance
================
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Converts RGB values to luminance.