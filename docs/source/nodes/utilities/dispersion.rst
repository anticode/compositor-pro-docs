Dispersion
==========
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Adds a dispersion effect to image, as seen in lenses.

Inputs
------
* Scale (Float)
    * Scale of the dispersion effect.
* Shape (Float)
    * Determines shape of the dispersion effect.
    * 0 means radial dispersion.
    * 1 means dispersion along the Y axis (left-right).
    * -1 means dispersion along the X axis (up-down).