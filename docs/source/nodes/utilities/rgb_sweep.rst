RGB Sweep
=========
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Outputs an RGB Sweep texture.