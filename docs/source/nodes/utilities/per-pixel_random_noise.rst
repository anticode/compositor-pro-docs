Per-pixel Random Noise
======================
:doc:`../../nodes`

Per-pixel random noise based on the resolution of the input image.