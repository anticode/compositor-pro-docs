RGB to BW
=========
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Converts RGB images to BW using max(R, G, B) function.