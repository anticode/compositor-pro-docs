Screen UV
=========
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Adds the screen UV texture of the current scene. Blue channel is a flat 1.