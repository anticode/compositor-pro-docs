Image Add
=========
:doc:`../../nodes`

An add operation that allows an RGB image to be a factor of the operation.