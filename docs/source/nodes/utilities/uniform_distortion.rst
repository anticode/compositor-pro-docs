Uniform Distortion
==================
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Lens distortion effect that does not stretch with the aspect ratio, akin to distortion seen in real lenses.

Inputs
------

* Input Scale (Float)
    * Scale of the input.
* Radial Scale (Float)
    * Scale of the output from the distorted radial effect.
* Distort (Float)
    * Amount of radial distortion to use before generating the ghost.