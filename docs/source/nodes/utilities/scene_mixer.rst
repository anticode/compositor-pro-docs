Scene Mixer
===========
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Mixes all of the different render passes into one combined pass with additional controls like AO factor and exposure controls.

Inputs
------
* AO Factor (Float)
    * Amount of additional AO to be factored into the image.
* Emission Exposure (Float)
    * Exposure of the emission pass.
    * Similar to scene exposure in Blender.
* Env Exposure (Float)
    * Exposure of the environment pass.
    * Similar to scene exposure in Blender.
* Bounce Exposure (Float)
    * Exposure of indirect passes.
    * Similar to scene exposure in Blender.