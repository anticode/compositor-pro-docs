Smart Denoiser
==============
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Denoises the image and mixes it with the noisy image using a noise threshold. This is to make sure non-noisy parts of the image don't look mushy because of the denoiser.

Inputs
------
* Noise Threshold (Float)
    * Determines the threshold to detect noise.