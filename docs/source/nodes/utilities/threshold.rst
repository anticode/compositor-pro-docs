Threshold
=========
:doc:`../../nodes`

.. note::
    This node is only available in the full version of Compositor Pro.

Thresholds the image based on parameters.

Inputs
------
* Threshold (Float)
    * Values below the threshold will be negated.
* Knee (Float)
    * Determines the falloff from the negated values to the accepted values.
    * 0 is a sharp cut.
    * 1 makes for a smooth transition.