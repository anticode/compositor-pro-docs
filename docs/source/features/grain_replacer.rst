Grain Replacer
==============
The Grain Replacer is visible on the side panel only while a :doc:`../nodes/mixed/grain` node is selected. Using the grain replacer will open the file browser for you to select a new grain texture to replace Compositor Pro's built-in grain texture with.