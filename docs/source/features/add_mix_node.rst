Add Mix Node
============
This adds a Mix node (sometimes referred to as Mix Color) much faster than you would be able to otherwise. You can also edit your Mix node before appending it by changing the dropdown and slider next to it, which correspond to mix type and fac respectively.

In order to make this tool even more useful, you can select nodes before clicking the Add button to connect them to the node as soon as its created. You can select one or two nodes, and the order of node selection depends on your :doc:`../preferences`.