Node Selector
=============
This selector allows you to select and import Compositor Pro nodes as needed. Imported nodes appear attached to your cursor so you can drag them into place immediately. On the node selector, you can also add a node to your favorites list so you can find it right next to all of your other favorites later. 

You can filter the selector by category (:doc:`../nodes`), as well as by :doc:`favorites` or :doc:`custom_nodes` or filter by search using the searchbar above the selector.

