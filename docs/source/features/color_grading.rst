Color Grading
=============
.. note::
    This feature is only available in the full version of Compositor Pro.

The color grading menu includes the Add Process feature, which works similarly to Add Mix Node. The dropdown menu on the left can be used to select your created process space and the right button to create it. This is most useful for isolating certain nodes or groups of nodes to get more accurate effects. You can also select a node to automatically create the process space around it, without having to edit it manually yourself.
