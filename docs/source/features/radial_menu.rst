Radial Menu
-----------
.. note::
    This feature is only available in the full version of Compositor Pro.

Compositor Pro has a radial menu that can be accessed with ``V`` by default (this can be changed in your :doc:`../preferences`) and includes several useful tools. On the right side of the menu, you can use the :doc:`add_mix_node` and :doc:`color_grading` menus without having to move to the side panel.

On the left side of the menu, you can see a shrunken version of the :doc:`node_selector` that is limited to the Favorites and :doc:`custom_nodes` menus.