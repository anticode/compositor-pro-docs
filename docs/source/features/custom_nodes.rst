Custom Nodes
============
.. note::
    This feature is only available in the full version of Compositor Pro.

As well as bringing all of its own nodes, Compositor Pro also allows you to make your own and import them quickly using all of our tools. Making a custom node can be done using the button on the side panel, which should then appear in a new "Custom Nodes" category in the :doc:`node_selector`. From the Node Selector, you can import your newly created node group, favorite it, or delete it.

If you are experiencing issues with custom nodes, you can use the Refresh Custom Nodes button to rebuild your custom nodes list. This is most useful if you've edited your custom nodes list in different instances of Blender at the same time, which creates a desync between the two instances.

Additionally you can use the Custom Node Exporter in the :doc:`preferences` menu to export a file that can be later imported using the button next to it. This is most useful for sharing your custom nodes between computers or updates of Compositor Pro.