Favorites
---------
Nodes can be favorited from the :doc:`node_selector`. Favorited nodes appear in their own category and can be imported like any other node. The same button used to favorite a node can also be used to remove a node from the favorites list. 