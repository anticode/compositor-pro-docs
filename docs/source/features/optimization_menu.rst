Optimizations Menu
==================
The Blender Compositor comes with incredibly convenient optimizations that are not enabled by default. If they are disabled, this menu will be visible to you in the side panel to automatically enable them for you.

Enable Optimizations makes the following changes:

* Disables OpenCL if its enabled.
    * OpenCL does not function correctly and causes issues with many Compositor Pro nodes.
    * This functionality will be changed if Blender ever fixes OpenCL.
* Enables Buffer Groups.
* Enables Two Pass.